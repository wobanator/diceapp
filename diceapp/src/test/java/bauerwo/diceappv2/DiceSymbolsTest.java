package bauerwo.diceappv2;

import org.junit.Test;

import bauerwo.diceappv2.diceLogic.DiceSymbol;
import bauerwo.diceappv2.diceLogic.DiceSymbols;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by bauerwo on 09.02.17.
 */

public class DiceSymbolsTest
{
    @Test
    public void equal_is_correct() throws Exception
    {
        assertEquals(new DiceSymbols(), new DiceSymbols());
        assertEquals(
                new DiceSymbols()
                .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 1))
                .addSymbol(new DiceSymbol(DiceSymbol.Type.Surge, 4))
                ,
                new DiceSymbols()
                .addSymbol(new DiceSymbol(DiceSymbol.Type.Surge, 4))
                .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 1))
                );
        assertEquals(
                new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 1))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Surge, 1))
                ,
                new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 1))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Surge, 1))
        );


        assertNotEquals(
                new DiceSymbols()
                .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 1))
                .addSymbol(new DiceSymbol(DiceSymbol.Type.X, 1))
                .addSymbol(new DiceSymbol(DiceSymbol.Type.Surge, 4))
                ,
                new DiceSymbols()
                .addSymbol(new DiceSymbol(DiceSymbol.Type.Surge, 4))
                .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 1))
                );
        assertNotEquals(
                new DiceSymbols()
                .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 1))
                .addSymbol(new DiceSymbol(DiceSymbol.Type.Surge, 3))
                ,
                new DiceSymbols()
                .addSymbol(new DiceSymbol(DiceSymbol.Type.Surge, 4))
                .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 1))
                );
    }

    @Test
    public void combine_is_correct() throws Exception
    {
        DiceSymbols s1 = new DiceSymbols()
            .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 1))
            .addSymbol(new DiceSymbol(DiceSymbol.Type.Surge, 4));
        DiceSymbols s2 = new DiceSymbols()
            .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 2))
            .addSymbol(new DiceSymbol(DiceSymbol.Type.Surge, 2));
        DiceSymbols s3 = new DiceSymbols()
            .addSymbol(new DiceSymbol(DiceSymbol.Type.X));
        DiceSymbols s4 = new DiceSymbols()
            .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 10))
            .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 1));

        assertEquals(DiceSymbols.combine(s1, s2), new DiceSymbols()
            .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 3))
            .addSymbol(new DiceSymbol(DiceSymbol.Type.Surge, 6)));

        assertEquals(DiceSymbols.combine(s2, s1), new DiceSymbols()
            .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 3))
            .addSymbol(new DiceSymbol(DiceSymbol.Type.Surge, 6)));

        assertEquals(DiceSymbols.combine(s1, s3), new DiceSymbols()
            .addSymbol(new DiceSymbol(DiceSymbol.Type.X)));

        assertEquals(DiceSymbols.combine(s1, s4), new DiceSymbols()
            .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 2))
            .addSymbol(new DiceSymbol(DiceSymbol.Type.Surge, 4))
            .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 10)));

        assertEquals(s1, new DiceSymbols()
                .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 1))
                .addSymbol(new DiceSymbol(DiceSymbol.Type.Surge, 4)));
        assertEquals(s2, new DiceSymbols()
                .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 2))
                .addSymbol(new DiceSymbol(DiceSymbol.Type.Surge, 2)));
        assertEquals(s3, new DiceSymbols()
                .addSymbol(new DiceSymbol(DiceSymbol.Type.X)));
        assertEquals(s4, new DiceSymbols()
                .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 10))
                .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 1)));
    }
}
