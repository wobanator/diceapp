package bauerwo.diceappv2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;
import java.util.ArrayList;

import bauerwo.diceappv2.diceLogic.Dice;
import bauerwo.diceappv2.diceLogic.DiceRolls;
import bauerwo.diceappv2.diceLogic.ShieldHeartDamage;

public class DiceSelectFragment extends Fragment {

    public static class ButtonIncrementListener implements View.OnClickListener
    {
        private TextView amount;

        public ButtonIncrementListener(TextView amount) { this.amount = amount; }

        @Override
        public void onClick(View v)
        {
            int value = Integer.parseInt(amount.getText().toString());
            value += 1;
            amount.setText(String.valueOf(value));
        }
    }

    public static class ButtonDecrementListener implements View.OnClickListener
    {
        private TextView amount;

        public ButtonDecrementListener(TextView amount) { this.amount = amount; }

        @Override
        public void onClick(View v)
        {
            int value = Integer.parseInt(amount.getText().toString());
            value -= 1;
            if (value >= 0) amount.setText(String.valueOf(value));
        }
    }

    public static class DiceSelect
    {
        Button plus_but;
        Button minus_but;
        TextView amount;
        Dice dice;

        public DiceSelect(View rootView, int plus_id, int minus_id, int amount_id, Dice dice) {

            this.dice = dice;

            plus_but = (Button) rootView.findViewById(plus_id);
            minus_but = (Button) rootView.findViewById(minus_id);
            amount = (TextView) rootView.findViewById(amount_id);

            plus_but.setOnClickListener(new ButtonIncrementListener(amount));
            minus_but.setOnClickListener(new ButtonDecrementListener(amount));
        }

        public DiceRolls getDiceRolls() {
            int value = Integer.parseInt(amount.getText().toString());
            if (value == 0) return null;
            else {
                DiceRolls rolls = dice.allRolls();
                for (int i = 1; i < value; i++) {
                    rolls = dice.multiplyWith(rolls);
                }
                return rolls;
            }
        }
    }

    private List<DiceSelect> diceSelectList;

    public DiceSelectFragment() {
        diceSelectList = new ArrayList<DiceSelect>();
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static DiceSelectFragment newInstance() {
        DiceSelectFragment fragment = new DiceSelectFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_decent_2, container, false);
        // TextView textView = (TextView) rootView.findViewById(R.id.section_label);
        // textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));

        DiceRegistry dices = DiceRegistry.getInstance();

        diceSelectList.add(new DiceSelect(rootView, R.id.red_plus, R.id.red_minus, R.id.red_amount, dices.red));
        diceSelectList.add(new DiceSelect(rootView, R.id.blue_plus, R.id.blue_minus, R.id.blue_amount, dices.blue));
        diceSelectList.add(new DiceSelect(rootView, R.id.yellow_plus, R.id.yellow_minus, R.id.yellow_amount, dices.yellow));
        diceSelectList.add(new DiceSelect(rootView, R.id.green_plus, R.id.green_minus, R.id.green_amount, dices.green));
        diceSelectList.add(new DiceSelect(rootView, R.id.brown_plus, R.id.brown_minus, R.id.brown_amount, dices.brown));
        diceSelectList.add(new DiceSelect(rootView, R.id.gray_plus, R.id.gray_minus, R.id.gray_amount, dices.gray));
        diceSelectList.add(new DiceSelect(rootView, R.id.black_plus, R.id.black_minus, R.id.black_amount, dices.black));

        return rootView;
    }

    public DiceRolls getDiceRolls() {
        DiceRolls rolls = null;
        for (DiceSelect sel: diceSelectList) {
            DiceRolls sel_rolls = sel.getDiceRolls();
            if (sel_rolls != null) {
                if (rolls == null) rolls = sel_rolls;
                else               rolls = rolls.multiplyWith(sel_rolls);
            }
        }
        if (rolls != null) rolls.applyDamage(new ShieldHeartDamage());
        return rolls;
    }

}
