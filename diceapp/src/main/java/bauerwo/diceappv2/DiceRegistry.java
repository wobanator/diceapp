package bauerwo.diceappv2;

import bauerwo.diceappv2.diceLogic.Dice;
import bauerwo.diceappv2.diceLogic.DiceSymbol;
import bauerwo.diceappv2.diceLogic.DiceSymbols;

public class DiceRegistry
{
    static private DiceRegistry instance;
    private DiceRegistry()
    {
        this.red = new Dice()
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 1)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 2)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 2)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 2)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 3)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 3))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Surge, 1)));

        this.blue = new Dice()
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.X)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 1))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Surge, 1))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 6)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 1))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 5)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 2))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 4)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 2))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 3)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 2))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Surge, 1))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 2)));

        this.yellow = new Dice()
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 1))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 1)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Surge, 1))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 1)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 1))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 2)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 2)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 1))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Surge, 1)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 2))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Surge, 1)));

        this.green = new Dice()
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 1)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 1))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 1)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Surge, 1)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Surge, 1))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 1)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 1))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Surge, 1)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 1))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Surge, 1))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 1)));

        this.gray = new Dice()
                .addSide(new DiceSymbols())
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Shield, 1)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Shield, 1)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Shield, 1)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Shield, 2)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Shield, 3)));

        this.brown = new Dice()
                .addSide(new DiceSymbols())
                .addSide(new DiceSymbols())
                .addSide(new DiceSymbols())
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Shield, 1)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Shield, 1)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Shield, 2)));

        this.black = new Dice()
                .addSide(new DiceSymbols())
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Shield, 2)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Shield, 2)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Shield, 2)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Shield, 3)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Shield, 4)));
    }

    public final Dice red;
    public final Dice blue;
    public final Dice yellow;
    public final Dice green;

    public final Dice gray;
    public final Dice brown;
    public final Dice black;

    static DiceRegistry getInstance()
    {
        if (instance == null) instance = new DiceRegistry();
        return instance;
    }
}
