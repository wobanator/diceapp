package bauerwo.diceappv2;

import bauerwo.diceappv2.diceLogic.Dice;
import bauerwo.diceappv2.diceLogic.DiceSymbol;
import bauerwo.diceappv2.diceLogic.DiceSymbols;

public class DoomDiceRegistry
{
    static private DoomDiceRegistry instance;
    private DoomDiceRegistry()
    {
        this.red = new Dice()
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.X, 1)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 1))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 3)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 3))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 1)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 2))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 2)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 3))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 1)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 4))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 1)));

        this.blue = new Dice()
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 1))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 1)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 2))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 0)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 2))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 0)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 3))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 0)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 3))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 0)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 2))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 1)));

        this.yellow = new Dice()
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.X, 1)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 1))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 3)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 3))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 1)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 1))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 3)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 2))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 2)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 4)));

        this.green = new Dice()
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 1))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 1)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 2)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 2)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 3)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 3)))
                .addSide(new DiceSymbols()
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Heart, 1))
                        .addSymbol(new DiceSymbol(DiceSymbol.Type.Number, 2)));
    }

    public final Dice red;
    public final Dice blue;
    public final Dice yellow;
    public final Dice green;

    static DoomDiceRegistry getInstance()
    {
        if (instance == null) instance = new DoomDiceRegistry();
        return instance;
    }
}
