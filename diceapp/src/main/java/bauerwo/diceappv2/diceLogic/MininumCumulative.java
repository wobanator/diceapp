package bauerwo.diceappv2.diceLogic;

public class MininumCumulative extends CumulativeBase
{
    public String toString()
    {
        return "Minimum Cumulative";
    }

    @Override
    public double getValue(int row, int col, IProbabilityRepresentation prop)
    {
        double sum = 0.0;
        for (int r = row; r < this.rowCount(prop); r++) {
            for (int c = col; c < this.colCount(prop); c++) {
                sum += prop.getValue(r, c, null);
            }
        }
        return sum;
    }

    @Override
    public String colLabel(int c, IProbabilityRepresentation prop) { return ">=" + prop.colLabel(c, null); }

    @Override
    public String rowLabel(int r, IProbabilityRepresentation prop) { return ">=" + prop.rowLabel(r, null); }
}
