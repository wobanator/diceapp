package bauerwo.diceappv2.diceLogic;


public class DirectRepresentation implements IProbabilityRepresentation {

    public String toString()
    {
        return "Direct Representation";
    }

    @Override
    public int rowCount(IProbabilityRepresentation prop) {
        return prop.rowCount(null);
    }

    @Override
    public int colCount(IProbabilityRepresentation prop) {
        return prop.colCount( null);
    }

    @Override
    public double getValue(int row, int col, IProbabilityRepresentation prop) {
        return prop.getValue(row, col, null);
    }

    @Override
    public String colLabel(int c, IProbabilityRepresentation prop) {
        return prop.colLabel(c, null);
    }

    @Override
    public String rowLabel(int r, IProbabilityRepresentation prop) {
        return prop.rowLabel(r, null);
    }
}
