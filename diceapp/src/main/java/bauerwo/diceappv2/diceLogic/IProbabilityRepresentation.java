package bauerwo.diceappv2.diceLogic;

public interface IProbabilityRepresentation
{
    public int rowCount(IProbabilityRepresentation prop);
    public int colCount(IProbabilityRepresentation prop);
    public double getValue(int row, int col, IProbabilityRepresentation prop);
    public String colLabel(int c, IProbabilityRepresentation prop);
    public String rowLabel(int r, IProbabilityRepresentation prop);
}
