package bauerwo.diceappv2.diceLogic;

import java.util.ArrayList;
import java.util.List;

import bauerwo.diceappv2.diceLogic.DiceSymbol;

public class DiceSymbols
{
    private List<DiceSymbol> symbols;

    public DiceSymbols() { this.symbols = new ArrayList<DiceSymbol>(); }

    public DiceSymbols(DiceSymbols other)
    {
        this.symbols = new ArrayList<DiceSymbol>(other.symbols.size());
        for (DiceSymbol s: other.symbols) {
            this.symbols.add(new DiceSymbol(s));
        }
    }

    public int size() { return this.symbols.size(); }
    public DiceSymbol get(int i) { return this.symbols.get(i); }
    public void add(DiceSymbol sym) { this.symbols.add(sym); }

    public DiceSymbol getSymbole(DiceSymbol.Type type)
    {
        for (DiceSymbol sym: this.symbols) {
            if (type == sym.type) return sym;
        }
        return null;
    }

    public DiceSymbol getSymbole(DiceSymbol sym) { return getSymbole(sym.type); }


   public DiceSymbols addSymbol(DiceSymbol sym)
   {
      DiceSymbol s = this.getSymbole(sym.type);
      if (s != null) {
         s.value += sym.value;
      } else {
         this.add(sym);
      }
      return this;
   }

    @Override
   public boolean equals(Object o)
   {
      DiceSymbols a = this;
      DiceSymbols b = (DiceSymbols) o;

      if (a.size() != b.size()) return false;

      for (int i = 0; i < a.size(); i++) {
         DiceSymbol sym_a = a.get(i);
         DiceSymbol sym_b = b.getSymbole(sym_a.type);
         if (sym_b == null) return false;
         if (sym_a.value != sym_b.value) return false;
      }
      return true;
   }

    @Override
   public int hashCode()
   {
      int hash = 0;
      for (int i = 0; i < this.size(); i++) {
         DiceSymbol symbol = this.get(i);
         hash += symbol.value << ((symbol.type.getValue() * 3) % 29);
      }
      return hash;
   }

   static public DiceSymbols combine(DiceSymbols a, DiceSymbols b)
   {
       // check for any X side
       if     (a.getSymbole(DiceSymbol.Type.X) != null ||
               b.getSymbole(DiceSymbol.Type.X) != null) {
           DiceSymbols x_res = new DiceSymbols();
           x_res.add(new DiceSymbol(DiceSymbol.Type.X));
           return x_res;
       } else {

           DiceSymbols result = new DiceSymbols(a);

           for (int i = 0; i < b.symbols.size(); i++) {
               result.addSymbol(b.symbols.get(i));
           }

           return result;
       }
   }
}