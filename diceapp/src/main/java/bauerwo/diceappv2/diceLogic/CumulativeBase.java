package bauerwo.diceappv2.diceLogic;

public abstract class CumulativeBase implements IProbabilityRepresentation
{
    @Override
    public int rowCount(IProbabilityRepresentation prop) { return prop.rowCount(null); }

    @Override
    public int colCount(IProbabilityRepresentation prop) { return prop.colCount(null); }
}
