package bauerwo.diceappv2.diceLogic;

public class DiceSymbol
{
   public enum Type
   {
      X(0), Heart(1), Surge(2), Shield(3), Number(4), Damage(5);

       private final int id;
       Type(int id) { this.id = id; }
       public int getValue() { return id; }
   }

   public DiceSymbol(Type type)
   {
      this.type = type;
      this.value = 1;
   }

   public DiceSymbol(Type type, int value)
   {
      this.type = type;
      this.value = value;
   }

   public DiceSymbol(DiceSymbol sym)
   {
      this.type = sym.type;
      this.value = sym.value;
   }

   public Type type; 
   public int  value;

};

