package bauerwo.diceappv2.diceLogic;

import java.util.List;

public class ProbabilityTable implements IProbabilityRepresentation
{
    private List<Integer> row_values;
    private List<Integer> col_values;

    private DiceSymbol.Type row_type;
    private DiceSymbol.Type col_type;

    private double [][] probabilities;

    public ProbabilityTable(DiceRolls rolls, List<Integer> row_values, DiceSymbol.Type row_type,
                                             List<Integer> col_values, DiceSymbol.Type col_type)
    {
        this.row_values = row_values;
        this.col_values = col_values;

        this.row_type = row_type;
        this.col_type = col_type;

        this.probabilities = new double[this.rowCount(null)][this.colCount(null)];

        if (row_type == col_type) {
            for (int row_nr = 0; row_nr < this.rowCount(null); row_nr++) {
                for (int col_nr = 0; col_nr < this.colCount(null); col_nr++) {
                    if (row_nr != col_nr) this.probabilities[row_nr][col_nr] = 0.0;
                    else {
                        int row = this.row_values.get(row_nr);
                        DiceSymbol row_sym = new DiceSymbol(this.row_type, row);
                        DiceSymbols symbs = new DiceSymbols();
                        symbs.add(row_sym);
                        this.probabilities[row_nr][col_nr] = rolls.getProbability(symbs);
                    }
                }
            }
        } else {
            for (int row_nr = 0; row_nr < this.rowCount(null); row_nr++) {
                int row = this.row_values.get(row_nr);
                DiceSymbol row_sym = new DiceSymbol(this.row_type, row);

                for (int col_nr = 0; col_nr < this.colCount(null); col_nr++) {
                    int col = this.col_values.get(col_nr);
                    DiceSymbol col_sym = new DiceSymbol(this.col_type, col);

                    DiceSymbols symbs = new DiceSymbols();
                    symbs.add(row_sym);
                    symbs.add(col_sym);

                    double prop = rolls.getProbability(symbs);

                    this.probabilities[row_nr][col_nr] = prop;
                }
            }
        }
    }

    public List<Integer> rowValues() { return this.row_values; }
    public List<Integer> colValues() { return this.col_values; }

    @Override
    public int rowCount(IProbabilityRepresentation p) { return this.row_values.size(); }

    @Override
    public int colCount(IProbabilityRepresentation p) { return this.col_values.size(); }

    @Override
    public double getValue(int row, int col, IProbabilityRepresentation p) { return this.probabilities[row][col]; }

    @Override
    public String colLabel(int c, IProbabilityRepresentation p) { return String.valueOf(this.col_values.get(c)); }

    @Override
    public String rowLabel(int r, IProbabilityRepresentation p) { return String.valueOf(this.row_values.get(r)); }
}
