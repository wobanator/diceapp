package bauerwo.diceappv2.diceLogic;


public interface IDamageApplyment
{
    public boolean hasDamage(DiceSymbols symbols);
    public void addDamage(DiceSymbols symbols);
}
