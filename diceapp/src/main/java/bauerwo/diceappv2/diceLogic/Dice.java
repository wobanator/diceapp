package bauerwo.diceappv2.diceLogic;

import java.util.ArrayList;
import java.util.List;

import bauerwo.diceappv2.diceLogic.DiceSymbols;

public class Dice
{
   private List<DiceSymbols> sides;

   public Dice() {
      this.sides = new ArrayList<DiceSymbols>();
   }

   public Dice addSide(DiceSymbols side) {
      this.sides.add(side);
      return this;
   }

   public DiceRolls allRolls() {
      double prop = 1.0 / sides.size();
      DiceRolls rolls = new DiceRolls();
      for (DiceSymbols side : sides) {
         rolls.add(new DiceSymbols(side), prop);
      }
      return rolls;
   }

   public DiceRolls multiplyWith(Dice otherDice) {
      return DiceRolls.multiply(this.allRolls(), otherDice.allRolls());
   }

   public DiceRolls multiplyWith(DiceRolls otherRolls) {
      return DiceRolls.multiply(this.allRolls(), otherRolls);
   }
}