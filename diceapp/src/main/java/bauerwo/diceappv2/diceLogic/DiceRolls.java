package bauerwo.diceappv2.diceLogic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DiceRolls
{
    private HashMap<DiceSymbols, Double> map;

    public DiceRolls()
    {
       this.map = new HashMap<DiceSymbols, Double>();
    }

    public void add(DiceSymbols key, double value)
    {
        Double entry = map.get(key);
        if (entry == null) map.put(key, value);
        else               map.put(key, entry + value);
    }

    public DiceRolls filter(Set<DiceSymbol.Type> symbolTypes)
    {
        DiceRolls ret = new DiceRolls();
        for (HashMap.Entry<DiceSymbols, Double> entry : this.map.entrySet())
        {
            DiceSymbols newSymbols = new DiceSymbols();
            for (DiceSymbol.Type t: symbolTypes) {
                DiceSymbol fromEntry = entry.getKey().getSymbole(t);
                if (fromEntry != null) newSymbols.add(new DiceSymbol(fromEntry));
                else                   newSymbols.add(new DiceSymbol(t, 0));
            }
            ret.add(newSymbols, entry.getValue());
        }
        return ret;
    }

    public List<Integer> getValuesFor(DiceSymbol.Type type)
    {
        HashSet<Integer> set = new HashSet<Integer>();
        for (DiceSymbols key: this.map.keySet()) {
            DiceSymbol sym = key.getSymbole(type);
            if (sym != null) set.add(sym.value);
        }
        List<Integer> list = new ArrayList<Integer>(set);
        Collections.sort(list);
        return list;
    }

    public Set<DiceSymbol.Type> getDiceSymbolTypes()
    {
        HashSet<DiceSymbol.Type> set = new HashSet<DiceSymbol.Type>();
        for (DiceSymbols symbols: this.map.keySet()) {
            for (int i = 0; i < symbols.size(); i++) {
                set.add(symbols.get(i).type);
            }
        }
        return set;
    }

    public double getProbability(DiceSymbols symbs)
    {
        Double prop = this.map.get(symbs);
        return (prop == null) ? 0.0 : prop;
    }


    public void applyDamage(IDamageApplyment damageApplyment)
    {
        HashMap<DiceSymbols, Double> new_map = new HashMap<DiceSymbols, Double>();

        for (HashMap.Entry<DiceSymbols, Double> entry: this.map.entrySet()) {
            DiceSymbols key = entry.getKey();

            damageApplyment.addDamage(key);

            new_map.put(key, entry.getValue());
        }
        this.map = new_map;
    }

    public DiceRolls multiplyWith(DiceRolls other) { return multiply(this, other); }

    static DiceRolls multiply(DiceRolls a, DiceRolls b)
    {
        DiceRolls ret = new DiceRolls();
        for (HashMap.Entry<DiceSymbols, Double> entry_a : a.map.entrySet())
        {
            for (HashMap.Entry<DiceSymbols, Double> entry_b : b.map.entrySet())
            {
                DiceSymbols combinedSymbols =
                        DiceSymbols.combine(entry_a.getKey(), entry_b.getKey());
                double combinedValue = entry_a.getValue() * entry_b.getValue();
                ret.add(combinedSymbols, combinedValue);
            }
        }
        return ret;
    }

    public ProbabilityTable getProbabilityTable(DiceSymbol.Type row_type, DiceSymbol.Type col_type)
    {
        DiceRolls filtered_rolls = this.filter(new HashSet<DiceSymbol.Type>(
                                                Arrays.asList(row_type, col_type)));

        List<Integer> row_values = filtered_rolls.getValuesFor(row_type);
        List<Integer> col_values = filtered_rolls.getValuesFor(col_type);

        return new ProbabilityTable(filtered_rolls, row_values, row_type, col_values, col_type);
    }
}
