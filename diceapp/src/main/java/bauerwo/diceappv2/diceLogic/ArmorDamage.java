package bauerwo.diceappv2.diceLogic;


public class ArmorDamage implements IDamageApplyment
{
    private int armor;

    public ArmorDamage(int armor)
    {
        this.armor = armor;
    }

    @Override
    public boolean hasDamage(DiceSymbols symbols)
    {
        return  symbols.getSymbole(DiceSymbol.Type.Heart) != null;
    }

    @Override
    public void addDamage(DiceSymbols symbols)
    {
        DiceSymbol hearts  = symbols.getSymbole(DiceSymbol.Type.Heart);

        if (hearts != null) {
            int damage = hearts.value / armor;
            if (damage < 0) damage = 0;
            symbols.add(new DiceSymbol(DiceSymbol.Type.Damage, damage));
        }
    }
}
