package bauerwo.diceappv2.diceLogic;


public class ShieldHeartDamage implements IDamageApplyment
{
    @Override
    public boolean hasDamage(DiceSymbols symbols)
    {
        return  symbols.getSymbole(DiceSymbol.Type.Heart) != null;
    }

    @Override
    public void addDamage(DiceSymbols symbols)
    {
        DiceSymbol hearts  = symbols.getSymbole(DiceSymbol.Type.Heart);
        DiceSymbol shields = symbols.getSymbole(DiceSymbol.Type.Shield);

        if (hearts != null) {
            int shields_amount = (shields == null) ? 0 : shields.value;
            int damage = hearts.value - shields_amount;
            if (damage < 0) damage = 0;
            symbols.add(new DiceSymbol(DiceSymbol.Type.Damage, damage));
        }
    }
}
