package bauerwo.diceappv2;

import android.annotation.TargetApi;
import android.database.DataSetObserver;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;

import bauerwo.diceappv2.diceLogic.DiceRolls;
import bauerwo.diceappv2.diceLogic.DiceSymbol;
import bauerwo.diceappv2.diceLogic.DiceSymbols;
import bauerwo.diceappv2.diceLogic.DirectRepresentation;
import bauerwo.diceappv2.diceLogic.IProbabilityRepresentation;
import bauerwo.diceappv2.diceLogic.MaximumCumulative;
import bauerwo.diceappv2.diceLogic.MininumCumulative;
import bauerwo.diceappv2.diceLogic.ShieldHeartDamage;

public class TableFragment extends Fragment {

    private DiceRolls diceRolls;
    private TableLayout tl;
    private Spinner row_spinner;
    private Spinner col_spinner;
    private Spinner style_spinner;

    private ArrayAdapter<DiceSymbol.Type> symbolsAdapter;
    private SpinnerAdapter stylesAdapter;
    private boolean ignorePropTableUpdate;

    public TableFragment() {
        ignorePropTableUpdate = false;
    }

    public static TableFragment newInstance() {
        TableFragment fragment = new TableFragment();
        return fragment;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_table, container, false);

        tl = (TableLayout) rootView.findViewById(R.id.table);
        row_spinner = (Spinner) rootView.findViewById(R.id.row_spinner);
        col_spinner = (Spinner) rootView.findViewById(R.id.col_spinner);
        style_spinner = (Spinner) rootView.findViewById(R.id.style_spinner);

        symbolsAdapter = new ArrayAdapter<DiceSymbol.Type>(inflater.getContext(),
                R.layout.support_simple_spinner_dropdown_item);

        row_spinner.setAdapter(symbolsAdapter);
        col_spinner.setAdapter(symbolsAdapter);


        style_spinner.setAdapter(new ArrayAdapter<IProbabilityRepresentation>(inflater.getContext(),
                R.layout.support_simple_spinner_dropdown_item,
                new IProbabilityRepresentation[]{
                    new MininumCumulative(), new MaximumCumulative(), new DirectRepresentation()
                }));


        AdapterView.OnItemSelectedListener updateFromSpinner = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                updatePropTable();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                updatePropTable();
            }
        };

        row_spinner.setOnItemSelectedListener(updateFromSpinner);
        col_spinner.setOnItemSelectedListener(updateFromSpinner);
        style_spinner.setOnItemSelectedListener(updateFromSpinner);



        return rootView;
    }

    public DiceSymbol.Type getSelectedRow() {return (DiceSymbol.Type) row_spinner.getSelectedItem(); }
    public DiceSymbol.Type getSelectedCol() {return (DiceSymbol.Type) col_spinner.getSelectedItem(); }
    public IProbabilityRepresentation getSelectedStyle() {return (IProbabilityRepresentation) style_spinner.getSelectedItem(); }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void updatePropTable() {
        if (!ignorePropTableUpdate) printPropTable(getSelectedRow(), getSelectedCol(), getSelectedStyle());
    }

    public void updateSpinners()
    {
        ignorePropTableUpdate = true;
        DiceSymbol.Type row_type = getSelectedRow();
        DiceSymbol.Type col_type = getSelectedCol();

        symbolsAdapter.clear();
        row_spinner.setSelection(-1);
        col_spinner.setSelection(-1);

        if (diceRolls != null) {
            Set<DiceSymbol.Type> symbolTypesSet = diceRolls.getDiceSymbolTypes();
            List<DiceSymbol.Type> symbolTypes = new ArrayList<DiceSymbol.Type>(symbolTypesSet);
            symbolsAdapter.addAll(symbolTypes);

            row_spinner.setSelection((symbolTypes.size() != 0) ? 0 : -1);
            col_spinner.setSelection((symbolTypes.size() != 0) ? 0 : -1);

            if (symbolTypesSet.contains(row_type))
                row_spinner.setSelection(symbolsAdapter.getPosition(row_type));
            if (symbolTypesSet.contains(col_type))
                col_spinner.setSelection(symbolsAdapter.getPosition(col_type));

            if (getSelectedRow() == getSelectedCol()) {
                for (int i = 0; i < symbolTypes.size(); i++) {
                    if (symbolTypes.get(i) != getSelectedRow()) {
                        col_spinner.setSelection(i);
                        break;
                    }
                }
            }
        }

        ignorePropTableUpdate = false;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void printPropTable(DiceSymbol.Type row_type, DiceSymbol.Type col_type,
                               IProbabilityRepresentation prop_pres)
    {
        tl.removeAllViews();
        if (this.diceRolls == null || row_type == null || col_type == null || prop_pres == null) return;

        IProbabilityRepresentation prop = this.diceRolls.getProbabilityTable(row_type, col_type);

        // add header
        {
            TableRow tr = new TableRow(getContext());
            tr.addView(this.newTableCell("", true));
            for (int c = 0; c < prop_pres.colCount(prop); c++) {
                tr.addView(this.newTableCell(prop_pres.colLabel(c, prop), true));
            }
            tl.addView(tr);
        }

        NumberFormat formatter = new DecimalFormat("#0.0000");
        for (int r = 0; r < prop_pres.rowCount(prop); r++) {

            TableRow tr = new TableRow(getContext());
            tr.addView(this.newTableCell(prop_pres.rowLabel(r, prop), true));

            for (int c = 0; c < prop_pres.colCount(prop); c++) {

                tr.addView(this.newTableCell(formatter.format(prop_pres.getValue(r, c, prop)), false));
            }
            tl.addView(tr);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public TextView newTableCell(String text, boolean header)
    {
        TextView cell = new TextView(getContext());
        cell.setBackground(ContextCompat.getDrawable(getContext(),
                (header) ? R.drawable.cell_header_shape
                        : R.drawable.cell_shape));
        cell.setText(text);
        cell.setTextSize(18);
        cell.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        cell.setPadding(5, 5, 5, 5);
        return cell;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void updateDiceRolls(DiceRolls diceRolls) {
        this.diceRolls = diceRolls;

        this.updateSpinners();
        this.printPropTable(getSelectedRow(), getSelectedCol(), getSelectedStyle());
    }
}
